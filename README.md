# Reaction-Diffusion-Displace

# MAE 2022 TP FINAL VIDA ARTIFICIAL (Emiliano Causa y Matías Romero Costas)
 
Espejo transformante a base de diferencia de  movimiento utilizando algoritmos de Reacción Difusión y Displace.

# [Video Explicativo](https://youtu.be/yg5G7fKpsTc)


 Creado:  2022/09/19

 Versión: 0.22

 
 ## Descripción: 
 utilizando las facilides de processing para construir una interfaz grafica 
 y manipular buffers de imagen y shaders, el programa realiza una sustracción de movimiento con el método de diferencia absoluta entre dos imágenes
 y luego esta información es procesada para realizar una deformación en la imagen de la cámara.
 
 por comodidad dejo a la vista la interfaz grafica y con presionar una tecla se cambia el modo de ver los buffers y la imagen resultante.
 
## Funcionamiento:
conjunto de 5 shaders en cascada, concatenando procesamiento en buffers 
 y luego bindiando la textura al siguiente shaders
 
### Esquema general 
* Cámara 
* buffer con un filtro de diferencia absoluta a una textura, en este caso a un buffer con el frame anterior de la cámara
* un buffer-filtro que suma y multiplica la imagen
*un buffer-filtro que amortigua la imagen linealmente y le agrega un threshhold
* un buffer filtro con la formula de  Karl Sims (https://www.karlsims.com/rd.html) 
 estudiada a partir de Simon Alexander-Adams (https://www.simonaa.media/tutorials/complex-systems-workshop) 
 y sol Sarratea (https://visualizer.solsarratea.world/04-gray-scott/) (https://github.com/solsarratea)
* y por ultimo un filtro de displace sobre la captura de la imagen en base al Autómata Celular (solo por comodidad puesto dentro de un buffer)

## Documentacion utilizada

* Karl Sims (https://www.karlsims.com/rd.html) 
* Simon Alexander-Adams (https://www.simonaa.media/tutorials/complex-systems-workshop) 
* sol Sarratea (https://visualizer.solsarratea.world/04-gray-scott/) (https://github.com/solsarratea)

 
## To Do:
* agregar archivo de configuración 
* save de un XML con el estado (no sobrescribiendo la config)
* migrar a OF, estabilizar o remplazar los shaders de la sustracción
* pasar a una experimentación con mas control ya no utilizar el operador laplaciano en 3 secciones (centro, adyacentes y diagonales )
 sino utilizar el modelo de rosa de los vientos teniendo 9 parámetros a controlar
* añadir un seteo de tamaño del AC en pixeles al shader
* agregar dos shaders y buffers para hacer un desplazamiento recursivo y coloreado
